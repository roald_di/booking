﻿namespace Booking.Benchmarks.V3
{
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    class SolutionV2 : ISolution
    {
        public string Solve(TextReader inputReader)
        {
            var keywords = inputReader.ReadLine();
            var reviews = new BlockingCollection<Review>();

            var compute = Task.Run(() =>
            {
                var index = new Index(keywords.Split(' '));
                var result = reviews
                    .GetConsumingEnumerable()
                    .AsParallel()
                    .Select(review => GetScore(review, index));

                var winners = result
                    .GroupBy(pair => pair.Id, (s, scores) => new Score(s, scores.Sum(score => score.Value)))
                    .OrderByDescending(pair => pair.Value)
                    .ThenBy(pair => pair.Id)
                    .ToArray();

                return string.Join(" ", winners.Select(pair => pair.Id));
            });

            var length = int.Parse(inputReader.ReadLine());
            for (var i = 0; i < length; i++)
            {
                reviews.Add(new Review(inputReader.ReadLine(), inputReader.ReadLine()));
            }

            reviews.CompleteAdding();

            return compute.Result;
        }

        static Score GetScore(Review review, Index index)
        {
            var indexIterator = index.GetIterator();
            var score = 0;

            foreach (var letter in review.Text)
            {
                switch (letter)
                {
                    case '.':
                    case ',':
                        continue;
                    case ' ':
                        if (indexIterator.IsEndOfWord)
                        {
                            score++;
                        }

                        indexIterator = index.GetIterator();
                        break;
                    default:
                        indexIterator.MoveNext(letter);
                        break;
                }
            }

            if (indexIterator.IsEndOfWord)
            {
                score++;
            }

            return new Score(review.Id, score);
        }
    }

    class Index
    {
        readonly IndexNode root = new IndexNode();

        public Index(IEnumerable<string> words)
        {
            foreach (var word in words)
            {
                Add(word);
            }
        }

        void Add(string word)
        {
            var current = root;

            foreach (var letter in word)
            {
                var nextIndex = TextExtensions.UnsafeCharIndex(letter);
                current = current.Children[nextIndex] ?? (current.Children[nextIndex] = new IndexNode());
            }

            current.IsEndOfWord = true;
        }

        public IndexIterator GetIterator() => new IndexIterator(root);
    }

    class IndexNode
    {
        public bool IsEndOfWord;
        public readonly IndexNode[] Children = new IndexNode[26];
    }

    class IndexIterator
    {
        IndexNode root;

        public IndexIterator(IndexNode root)
        {
            this.root = root;
        }

        public bool IsEndOfWord => root != null && root.IsEndOfWord;

        public bool MoveNext(char letter)
        {
            if (root == null)
            {
                return false;
            }

            root = root.Children[TextExtensions.UnsafeCharIndex(letter)];
            return root != null;
        }
    }

    struct Review
    {
        public Review(string id, string text)
        {
            Id = id;
            Text = text;
        }

        public readonly string Id;
        public readonly string Text;
    }

    struct Score
    {
        public Score(string id, int value)
        {
            Id = id;
            Value = value;
        }

        public readonly string Id;
        public readonly int Value;
    }

    static class TextExtensions
    {
        public static int UnsafeCharIndex(char letter) 
            => letter - ('a' <= letter && letter <= 'z' ? 'a' : 'A');
    }
}