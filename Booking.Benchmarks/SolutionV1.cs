﻿namespace Booking.Benchmarks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    class SolutionV1 : ISolution
    {
        public string Solve(TextReader inputReader)
        {
            var keywords = new HashSet<string>();
            foreach (var word in inputReader.ReadLine().ToUpper().Split(' '))
                keywords.Add(word);

            var numReviews = int.Parse(inputReader.ReadLine());
            var reviews = new Dictionary<string, int>();
            for (var i = 0; i < numReviews; i++)
            {
                var id = inputReader.ReadLine();
                var review = inputReader.ReadLine();

                var matches = 0;
                foreach (var word in review.ToUpper().Split(new[] { '.', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (keywords.Contains(word))
                    {
                        matches++;
                    }
                }

                if (!reviews.ContainsKey(id)) reviews[id] = 0;
                reviews[id] += matches;
            }
            var output = string.Join(" ",
                reviews.OrderByDescending((arg) => arg.Value).ThenBy((arg) => arg.Key).Select(a => a.Key).ToArray());

            return output;
        }
    }
}