﻿using System.Text;
using System.Threading.Tasks;

namespace Booking.Benchmarks
{
    using System.IO;

    interface ISolution
    {
        string Solve(TextReader inputReader);
    }
}
