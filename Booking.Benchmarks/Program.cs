﻿namespace Booking.Benchmarks
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using V3;

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("generating...");
            var words = File.ReadAllLines(@"..\..\..\words.txt");
            var input = Generate(
                numKeywords: 100,
                numReviews: 10000,
                numIds: 100,
                reviewLength: 10000,
                dictionary: words);
            File.WriteAllText(@"..\..\..\input.txt", input);

            var warmup = 0;
            var iterations = 1;

            var solutions = new Dictionary<string, ISolution>
            {
                {"v1", new SolutionV1() },
                {"v3", new SolutionV2() }
            };

            Console.WriteLine("started");

            var results = new List<string>();
            foreach (var solution in solutions)
            {
                var result = BenchMark(() => solution.Value.Solve(new StringReader(input)), warmup, iterations);
                if (result.Item2.Length <= 150)
                {
                    Console.WriteLine(result.Item2);
                }

                Console.WriteLine($"{solution.Key} completed in {result.Item1}ms.");
                results.Add(result.Item2);
            }

            var first = results[0];
            for (int index = 0; index < results.Count; index++)
            {
                if (results[index] != first)
                {
                    var color = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"result {index} not equal to first.");
                    Console.WriteLine("expected:");
                    Console.WriteLine(first);
                    Console.WriteLine("but was:");
                    Console.WriteLine(results[index]);
                    Console.ForegroundColor = color;
                }
            }
        }

        static Tuple<long, string> BenchMark(Func<string> func, int warmup, int iterations)
        {
            for (var i = 0; i < warmup; i++)
            {
                func();
            }

            var watch = new Stopwatch();
            watch.Start();
            for (var i = 0; i < iterations - 1; i++)
            {
                func();
            }

            var result = func();
            watch.Stop();

            return Tuple.Create(watch.ElapsedMilliseconds / iterations, result);
        }

        static string Generate(int numKeywords, int numReviews, int numIds, int reviewLength, IReadOnlyList<string> dictionary)
        {
            var random = new Random(99999);

            var output = new StringBuilder();
            var keywords = Enumerable.Repeat(0, numKeywords)
                .Select(_ => dictionary[random.Next(0, dictionary.Count)]);

            output.AppendLine(string.Join(" ", keywords));
            output.AppendLine(numReviews.ToString());

            var ids = Enumerable.Range(0, numIds)
                .Select(id => id.ToString())
                .ToArray();

            for (var i = 0; i < numReviews; i++)
            {
                output.AppendLine(ids[random.Next(0, ids.Length)]);

                for (var j = 0; j < reviewLength; j++)
                {
                    output.Append(dictionary[random.Next(0, dictionary.Count)]);

                    if (j < reviewLength - 1)
                    {
                        output.Append(" ");
                    }
                }

                output.AppendLine();
            }

            return output.ToString();
        }
    }
}